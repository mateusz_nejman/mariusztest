import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import appConfigReducer from './app-config';

const rootReducer = history => combineReducers({
    router: connectRouter(history),
    app: appConfigReducer
})

export default rootReducer;