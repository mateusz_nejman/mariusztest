import React from 'react';
import LayoutPanel from './layout-panel';
import { Switch, Route } from 'react-router-dom';
import DashboardPage from './pages/panel/dashboard';
import OrdersPage from './pages/panel/orders';
import BrokersPage from './pages/panel/brokers';

function App() {
  return (
    <LayoutPanel>
      <Switch>
        <Route path="/panel/dashboard">
          <DashboardPage/>
        </Route>
        <Route path="/panel/orders">
          <OrdersPage/>
        </Route>
        <Route path="/panel/brokers">
          <BrokersPage/>
        </Route>
      </Switch>
    </LayoutPanel>
  );
}

export default App;
