import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Dashboard, ThumbsUpDown, Group, Description, Settings, List as ListIcon, Map, Extension, ArrowBack, Menu as MenuIcon} from "@material-ui/icons";
import { faFlask, faVials, faPrescriptionBottle, faBoxes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faAmilia} from "@fortawesome/free-brands-svg-icons";
import { Link } from 'react-router-dom';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    color: "white",
    backgroundColor: "#9a0000"
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#9a0000"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  list: {
      color: "white"
  }
}));

function LayoutPanel(props) {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List className={classes.list}>
          <ListItem button component={Link} to="/panel/dashboard">
              <ListItemIcon><Dashboard style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Tablica"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/orders">
              <ListItemIcon><ThumbsUpDown style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Zamówienia"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/allegro">
              <ListItemIcon><FontAwesomeIcon icon={faAmilia} color="#FFFFFF"/></ListItemIcon>
              <ListItemText primary="Allegro"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/brokers">
              <ListItemIcon><Group style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Pośrednicy"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/invoices">
              <ListItemIcon><Description style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Faktury"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/settings">
              <ListItemIcon><Settings style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Ustawienia"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/todo">
              <ListItemIcon><ListIcon style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="TO-DO"/>
          </ListItem>
      </List>
      <Divider />
      <List className={classes.list}>
      <ListItem button component={Link} to="/panel/compounds">
              <ListItemIcon><FontAwesomeIcon icon={faFlask} color="#FFFFFF"/></ListItemIcon>
              <ListItemText primary="Związki"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/solutions">
              <ListItemIcon><FontAwesomeIcon icon={faVials} color="#FFFFFF"/></ListItemIcon>
              <ListItemText primary="Roztwory"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/magazine">
              <ListItemIcon><ListIcon style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Stan Magazynowy"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/products">
              <ListItemIcon><FontAwesomeIcon icon={faBoxes} color="#FFFFFF"/></ListItemIcon>
              <ListItemText primary="Produkty"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/containers">
              <ListItemIcon><FontAwesomeIcon icon={faPrescriptionBottle} color="#FFFFFF"/></ListItemIcon>
              <ListItemText primary="Pojemniki"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/map">
              <ListItemIcon><Map style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Mapa Magazynu"/>
          </ListItem>
          <ListItem button component={Link} to="/panel/api">
              <ListItemIcon><Extension style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Api"/>
          </ListItem>
          <ListItem button component={Link} to="/">
              <ListItemIcon><ArrowBack style={{color: "#FFFFFF"}}/></ListItemIcon>
              <ListItemText primary="Wróć na stronę"/>
          </ListItem>
      </List>
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            Protolab System
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {props.children}
      </main>
    </div>
  );
}

LayoutPanel.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.instanceOf(typeof Element === 'undefined' ? Object : Element),
};

export default LayoutPanel;