import React from "react";
import { Typography, Paper, Breadcrumbs, Link, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  component: {
    margin: theme.spacing(1)
  },
  root: {
    padding: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  breadcrumb: {
    marginBottom: theme.spacing(1)
  },
  topPanelButton: {
    marginLeft: "auto"
  }
}));

export default function BrokersPage(props) {
  const classes = useStyles();

  return (
    <Typography component="div">
      <Paper className={classes.root}>
        <Breadcrumbs aria-label="breadcrumb" className={classes.breadcrumb}>
          <Link color="inherit" href="/">
            Protolab System
          </Link>
          <Typography color="textPrimary">Pośrednicy</Typography>
        </Breadcrumbs>
        <Button
          variant="contained"
          className={classes.topPanelButton}
        >
          Dodaj
        </Button>
      </Paper>
      <Paper className={classes.root}>
      </Paper>
    </Typography>
  );
}
