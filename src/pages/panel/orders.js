import React from "react";
import {
  Typography,
  Paper,
  Breadcrumbs,
  Link,
  Button,
  useMediaQuery,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Grid,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  FormControlLabel,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";

const useStyles = makeStyles(theme => ({
  component: {
    margin: theme.spacing(1)
  },
  root: {
    padding: theme.spacing(3, 2),
    marginBottom: theme.spacing(1)
  },
  tables: {
    marginBottom: theme.spacing(1)
  },
  topPanel: {
    display: "flex",
    flexDirection: "row",
    padding: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  topPanelButton: {
    marginLeft: "auto"
  },
  topPanelButtonSecond: {
    marginLeft: theme.spacing(1)
  },
  modalGrid: {
    display: "flex",
    flexDirection: "column"
  },
  customInput: {
    margin: theme.spacing(1)
  },
  customProduct: {
    display: "flex",
    flexDirection: "row",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column"
    },
    padding: theme.spacing(1)
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  card: {
    height: "100%",
    margin: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  gridContainer: {
    margin: theme.spacing(1)
  }
}));

const columns = ["ID", "Nazwa odbiorcy", "Data", "Status", "Szczegóły"];

const columns1 = ["Nazwa produktu", "Liczba", "Cena za 1szt", "Cena", "Usuń"];

const data = [
  ["Joe James", "Test Corp", "Yonkers", "NY", "NY"],
  ["John Walsh", "Test Corp", "Hartford", "CT", "NY"],
  ["Bob Herm", "Test Corp", "Tampa", "FL", "NY"],
  ["James Houston", "Test Corp", "Dallas", "TX", "NY"]
];

const data1 = [["Test", 2, 2.4, 4.8, "Usuń"]];

const options = {
  filter: true,
  filterType: null,
  responsive: "scrollMaxHeight",
  selectableRows: "none",
  download: false,
  print: false,
  textLabels: {
    body: {
      noMatch: "Brak danych",
      toolTip: "Sortuj",
      columnHeaderTooltip: column => `Sortuj po ${column.label}`
    },
    pagination: {
      next: "Następny",
      previous: "Poprzedni",
      rowsPerPage: "Produktów na stronę:",
      displayRows: "z"
    },
    toolbar: {
      search: "Szukaj",
      downloadCsv: "Pobierz",
      print: "Drukuj",
      viewColumns: "Pokaż kolumny",
      filterTable: "Filtruj"
    },
    filter: {
      all: "Wszystkie",
      title: "Filtruj",
      reset: "Resetuj"
    },
    viewColumns: {
      title: "Pokaż kolumny",
      titleAria: "Pokaż kolumny"
    }
  }
};

export default function OrdersPage(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [addModalOpen, setAddModalOpen] = React.useState(false);
  const [editModalOpen, setEditModalOpen] = React.useState(false);
  const fullscreen = useMediaQuery(theme.breakpoints.down("sm"));
  const maxWidth = "lg";

  const [orderFirstName, setOrderFirstName] = React.useState("");
  const [orderSurName, setOrderSurName] = React.useState("");
  const [orderAddress, setOrderAddress] = React.useState("");
  const [orderZip, setOrderZip] = React.useState("");
  const [orderCity, setOrderCity] = React.useState("");
  const [orderCountry, setOrderCountry] = React.useState("");
  const [orderPhone, setOrderPhone] = React.useState("");
  const [orderCompany, setOrderCompany] = React.useState("");
  const [orderNip, setOrderNip] = React.useState("");
  const [orderMethod, setOrderMethod] = React.useState(0);
  const [orderDate, setOrderDate] = React.useState("13-12-2019");
  const [orderDateV, setOrderDateV] = React.useState("");

  const [invoiceFirstName, setInvoiceFirstName] = React.useState("");
  const [invoiceSurName, setInvoiceSurName] = React.useState("");
  const [invoiceAddress, setInvoiceAddress] = React.useState("");
  const [invoiceZip, setInvoiceZip] = React.useState("");
  const [invoiceCity, setInvoiceCity] = React.useState("");
  const [invoiceCountry, setInvoiceCountry] = React.useState("");
  const [invoiceCompany, setInvoiceCompany] = React.useState("");
  const [invoiceNip, setInvoiceNip] = React.useState("");

  const [invoiceAnother, setInvoiceAnother] = React.useState(false);
  const [invoiceDisabled, setInvoiceDisabled] = React.useState("disabled");

  const bull = <span className={classes.bullet}>•</span>;

  const handleAddModalOpen = () => {
    setAddModalOpen(true);
  };

  const handleAddModalClose = () => {
    setAddModalOpen(false);
  };

  const handleEditModalOpen = () => {
    setEditModalOpen(true);
  };
  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  return (
    <Typography component="div">
      <Paper className={classes.topPanel}>
        <Breadcrumbs aria-label="breadcrumb" className={classes.breadcrumb}>
          <Link color="inherit" href="/">
            Protolab System
          </Link>
          <Typography color="textPrimary">Zamówienia</Typography>
        </Breadcrumbs>
        <Button
          variant="contained"
          className={classes.topPanelButton}
          onClick={handleAddModalOpen}
        >
          Dodaj
        </Button>
        <Button
          variant="contained"
          className={classes.topPanelButtonSecond}
          onClick={handleEditModalOpen}
        >
          Edytuj
        </Button>
      </Paper>
      <Paper className={classes.root}>
        <Typography variant="h5" component="h3">
          Zamówienia
        </Typography>
        <Typography component="div" className={classes.tables}>
          <MUIDataTable
            title={"Zamówienia w trakcie i inne"}
            data={data}
            columns={columns}
            options={options}
          />
        </Typography>
        <Typography component="div" className={classes.tables}>
          <MUIDataTable
            title={"Zamówienia wysłane i odebrane"}
            data={data}
            columns={columns}
            options={options}
          />
        </Typography>
      </Paper>

      <Dialog
        fullScreen={fullscreen}
        open={addModalOpen}
        onClose={handleAddModalClose}
        aria-labelledby="responsive-dialog-title"
      >
        <form autoComplete="off">
          <DialogTitle id="responsive-dialog-title">
            {"Dodaj zamówienie(produkty będziesz mógł dodać później)"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <Grid container spacing={3}>
                <Grid item xs={12} sm={6} className={classes.modalGrid}>
                  <Typography variant="h6">Dane do wysyłki</Typography>
                  <TextField
                    id="order-firstname"
                    label="Imię (Wymagane)"
                    value={orderFirstName}
                    onChange={event => setOrderFirstName(event.target.value)}
                    required
                  />
                  <TextField
                    id="order-surname"
                    label="Nazwisko (Wymagane)"
                    value={orderSurName}
                    onChange={event => setOrderSurName(event.target.value)}
                    required
                  />
                  <TextField
                    id="order-phone"
                    label="Telefon (Wymagane)"
                    value={orderPhone}
                    onChange={event => setOrderPhone(event.target.value)}
                    required
                  />
                  <TextField
                    id="order-address"
                    label="Adres (Wymagane)"
                    value={orderAddress}
                    onChange={event => setOrderAddress(event.target.value)}
                    required
                  />
                  <TextField
                    id="order-zip"
                    label="Kod (Wymagane)"
                    value={orderZip}
                    onChange={event => setOrderZip(event.target.value)}
                    required
                  />
                  <TextField
                    id="order-city"
                    label="Miasto (Wymagane)"
                    value={orderCity}
                    onChange={event => setOrderCity(event.target.value)}
                    required
                  />
                  <TextField
                    id="order-country"
                    label="Kraj (Wymagane)"
                    value={orderCountry}
                    onChange={event => setOrderCountry(event.target.value)}
                    required
                  />
                  <TextField
                    id="order-company"
                    label="Nazwa firmy (Opcjonalnie)"
                    value={orderCompany}
                    onChange={event => setOrderCompany(event.target.value)}
                  />
                  <TextField
                    id="order-nip"
                    label="Nip"
                    value={orderNip}
                    onChange={event => setOrderNip(event.target.value)}
                  />
                  <FormControl className={classes.formControl}>
                    <InputLabel shrink id="order-method-label">
                      Sposób płatności
                    </InputLabel>
                    <Select
                      labelId="order-method-label"
                      id="order-method"
                      displayEmpty
                      className={classes.selectEmpty}
                      value={orderMethod}
                      onChange={event => {
                        setOrderMethod(event.target.value);
                        if (event.target.value === 0) setOrderDateV("");
                        else setOrderDateV("disabled");
                      }}
                    >
                      <MenuItem value={0}>
                        <em>Przelew</em>
                      </MenuItem>
                      <MenuItem value={1}>Pobranie</MenuItem>
                      <MenuItem value={2}>Przedpłata</MenuItem>
                    </Select>
                  </FormControl>
                  <TextField
                    id="order-date"
                    label="Termin płatności"
                    value={orderDate}
                    onChange={event => setOrderDate(event.target.value)}
                    disabled={orderDateV}
                  />
                </Grid>
                <Grid item xs={12} sm={6} className={classes.modalGrid}>
                  <Typography variant="h6">Dane do faktury</Typography>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={invoiceAnother}
                        onChange={event => {
                          setInvoiceAnother(event.target.checked);
                          setInvoiceDisabled(invoiceAnother ? "disabled" : "");
                        }}
                      />
                    }
                    label="Dane inne niż do przesyłki"
                  />

                  <TextField
                    id="invoice-firstname"
                    label="Imię"
                    value={invoiceFirstName}
                    onChange={event => setInvoiceFirstName(event.target.value)}
                    disabled={invoiceDisabled}
                  />
                  <TextField
                    id="invoice-surname"
                    label="Nazwisko"
                    value={invoiceSurName}
                    onChange={event => setInvoiceSurName(event.target.value)}
                    disabled={invoiceDisabled}
                  />
                  <TextField
                    id="invoice-address"
                    label="Adres (Wymagane)"
                    value={invoiceAddress}
                    onChange={event => setInvoiceAddress(event.target.value)}
                    required
                    disabled={invoiceDisabled}
                  />
                  <TextField
                    id="invoice-zip"
                    label="Kod (Wymagane)"
                    value={invoiceZip}
                    onChange={event => setInvoiceZip(event.target.value)}
                    required
                    disabled={invoiceDisabled}
                  />
                  <TextField
                    id="invoice-city"
                    label="Miasto (Wymagane)"
                    value={invoiceCity}
                    onChange={event => setInvoiceCity(event.target.value)}
                    required
                    disabled={invoiceDisabled}
                  />
                  <TextField
                    id="invoice-country"
                    label="Kraj (Wymagane)"
                    value={invoiceCountry}
                    onChange={event => setInvoiceCountry(event.target.value)}
                    required
                    disabled={invoiceDisabled}
                  />
                  <TextField
                    id="invoice-company"
                    label="Nazwa firmy"
                    value={invoiceCompany}
                    onChange={event => setInvoiceCompany(event.target.value)}
                    disabled={invoiceDisabled}
                  />
                  <TextField
                    id="invoice-nip"
                    label="Nip"
                    value={invoiceNip}
                    onChange={event => setInvoiceNip(event.target.value)}
                    disabled={invoiceDisabled}
                  />
                </Grid>
              </Grid>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleAddModalClose} color="primary">
              Anuluj
            </Button>
            <Button color="primary" autoFocus type="submit">
              Dodaj
            </Button>
          </DialogActions>
        </form>
      </Dialog>
      <Dialog
        fullScreen={fullscreen}
        open={editModalOpen}
        onClose={handleEditModalClose}
        fullWidth={true}
        maxWidth={maxWidth}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          {"Edytuj zamówienie"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Typography component="div" className={classes.tables}>
              <MUIDataTable
                title={"Lista produktów"}
                data={data1}
                columns={columns1}
                options={options}
              />
            </Typography>
            <Typography variant="h6">Niestandardowa pozycja</Typography>
            <Typography className={classes.customProduct} component="div">
              <TextField
                id="custom-name"
                label="Nazwa produktu"
                className={classes.customInput}
              />
              <TextField
                id="custom-count"
                label="Liczba"
                className={classes.customInput}
              />
              <TextField
                id="custom-cost1"
                label="Cena za 1szt"
                className={classes.customInput}
              />
              <TextField
                id="custom-cost"
                label="Cena łączna"
                className={classes.customInput}
              />
              <Button variant="contained" className={classes.customInput}>
                Dodaj
              </Button>
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <Card className={classes.card}>
                  <CardContent>
                    <Typography variant="h5" component="h2">
                      Dane wysyłki
                    </Typography>
                    <Typography
                      className={classes.title}
                      color="textSecondary"
                      gutterBottom
                    >
                      Mateusz Nejman
                    </Typography>

                    <Typography className={classes.pos} color="textSecondary">
                      Starzyńskiego 8/1a, 76-200 Słupsk
                    </Typography>
                    <Typography variant="body2" component="p">
                      Tel: 575882078
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                      Firma: Brak
                      <br />
                      {"Nip: Brak"}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Card className={classes.card}>
                  <CardContent>
                    <Typography variant="h5" component="h2">
                      Dane do faktury
                    </Typography>
                    <Typography
                      className={classes.title}
                      color="textSecondary"
                      gutterBottom
                    >
                      Nazwa firmy: Brak
                    </Typography>

                    <Typography className={classes.pos} color="textSecondary">
                      Nip: Brak
                      <br />
                      Adres: Starzyńskiego 8/1a, 76-200 Słupsk
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small">Pobierz</Button>
                    <Button size="small">Otwórz</Button>
                  </CardActions>
                </Card>
              </Grid>
              <Grid item xs={12}>
                <Card className={classes.card}>
                  <CardContent>
                    <Typography variant="h5" component="h2">
                      Inne dane
                    </Typography>
                    <Typography
                      className={classes.title}
                      color="textSecondary"
                      gutterBottom
                    >
                      Data zamówienia: 17.12.2019
                    </Typography>

                    <Typography className={classes.pos} color="textSecondary">
                      Status: Do opłacenia
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item xs={12}>
                <Typography component="div" className={classes.tables}>
                  <MUIDataTable
                    title={"Dodaj produkty"}
                    data={data}
                    columns={columns}
                    options={options}
                  />
                </Typography>
              </Grid>
            </Grid>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleEditModalClose} color="primary">
            Anuluj
          </Button>
          <Button color="primary" autoFocus type="submit">
            Edytuj
          </Button>
        </DialogActions>
      </Dialog>
    </Typography>
  );
}
