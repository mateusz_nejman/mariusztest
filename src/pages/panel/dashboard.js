import React from "react";
import { Typography, Paper, Breadcrumbs, Link } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Chart } from "react-google-charts";

const useStyles = makeStyles(theme => ({
    component: {
        margin: theme.spacing(1)
    },
    root: {
        padding: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    breadcrumb: {
        marginBottom: theme.spacing(1)
    }
}));

export default function DashboardPage(props) {
    const classes = useStyles();

    return (
        <Typography component="div">
            <Paper className={classes.root}><Breadcrumbs aria-label="breadcrumb" className={classes.breadcrumb}>
                <Link color="inherit" href="/" >Protolab System</Link>
                <Typography color="textPrimary">Tablica</Typography>
            </Breadcrumbs></Paper>
            <Paper className={classes.root}>
                <Typography variant="h5" component="h3">Miesięczny zarobek</Typography>
                <Typography component="p">
                    <Chart
                        width={"98%"}
                        height={"98%"}
                        chartType="Line"
                        loader={<div>Loading Chart</div>}
                        data={[["Data", "Zarobek"], ["13-12-2019", 2500], ["14-12-2019", 1500]]}
                        options={{
                            chartArea: { width: '50%', height: '70%' },
                        }}
                    />
                </Typography>
            </Paper>
        </Typography>

    );
}
